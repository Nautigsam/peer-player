'use strict';

var watchify = require('watchify');
var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var browserifyHandlebars = require('browserify-handlebars');

var customOpts = {
    entries: ['./src/js/app.js'],
    transform: [browserifyHandlebars]
};

var bundle = function (b) {
    return b.bundle()
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    //.pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/js'));
};

gulp.task('bundle', () => {
    return bundle(browserify(customOpts));
});
gulp.task('watch', () => {
    let options = Object.assign({}, watchify.args, customOpts);
    let b = watchify(browserify(options));
    b.on('update', () => bundle(b));
    b.on('log', gutil.log);
    return bundle(b);
});
gulp.task('default', ['bundle']);