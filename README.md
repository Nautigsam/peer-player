# Peer-player
Some experiments with WebRTC.

### Getting started
- `npm install -g gulp`
- `npm install`
- `npm start`
- Join a room by visiting this URL, per example: `http://localhost:3000/helloRoom`

### What is it working for now?

- Multi-user chat separated in different rooms.
