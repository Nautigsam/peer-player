'use strict';

var server = module.exports = require('./server/server');

if (require.main === module) {
    let port = 3000;
    server.listen(port, () => {
        console.log(`Server started. Access it at http://localhost:${port}`);
    });
    process.on('exit', () => {
        server.close();
    });
}