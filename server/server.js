'use strict';

var randomstring = require('randomstring');
var BinarySearchTree = require('binary-search-tree').BinarySearchTree;
var http = require('http');
var WSServer = require('ws').Server;
var express = require('express');
var app = express();

var server = module.exports = http.createServer(app);
var wss = new WSServer({server});

var userIdCounter = 1;
var rooms = {};
var connections = {};

var getNewUserId = () => {
    if (app.get('env') === 'production') {
        return Promise.race([
            new Promise((resolve) => {
                let id;
                let collisions = -1;
                do {
                    collisions++;
                    id = randomstring.generate(32);
                } while (connections.hasOwnProperty(id));
                if (collisions > 0) console.error(`[Server] Got user id after ${collisions} attempts`);
                resolve(id);
            }),
            new Promise((resolve, reject) => {
                setTimeout(() => {
                    reject(new Error(`Too much waiting for getting a new user id`));
                }, 10000);
            })
        ]);
    } else {
        return Promise.resolve(userIdCounter++);
    }
};

var roomExists = (roomId) => {
    return rooms.hasOwnProperty(roomId);
};
var userExists = (roomId, userId) => {
    return roomExists(roomId) && (rooms[roomId].initiator === userId || rooms[roomId].users.search(userId).length > 0);
};

// get all keys of a BinarySearchTree
var _appendArrays = (outArray, inArray) => {
    for (let e of inArray) outArray.push(e);
};
var getAllBSTKeys = (bst) => {
    let keys = [];
    if (!bst.hasOwnProperty('key')) return [];
    if (bst.left) _appendArrays(keys, getAllBSTKeys(bst.left));
    keys.push(bst.key);
    if (bst.right) _appendArrays(keys, getAllBSTKeys(bst.right));
    return keys;
};

wss.on('connection', (ws) => {
    getNewUserId()
    .then((userId) => {
        ws.send(JSON.stringify({key: 'accept', userId}));
        connections[userId] = {ws, rooms: []};
        console.log(`[WS] user "${userId}" is connected`);
        ws.on('message', (messageStr) => {
            console.log(`[WS] Message to user "${userId}": "${messageStr}"`);
            let message = JSON.parse(messageStr);
            switch (message.key) {
                case 'enterRoom':
                    let sendWelcomeMessage = (room) => {
                        connections[userId].ws.send(JSON.stringify({key: 'welcome', message: room.welcomeMessage}));
                    };
                    if (roomExists(message.roomId)) {
                        if (!userExists(message.roomId, userId)) {
                            let room = rooms[message.roomId];
                            sendWelcomeMessage(room);
                            let initiatorNeighbors = room.users.search(room.initiator)[0];
                            initiatorNeighbors.insert(userId);
                            room.users.insert(userId, new BinarySearchTree({unique: true}));
                            connections[room.initiator].ws.send(JSON.stringify({key: 'newUser', userId}));
                        }
                    } else {
                        let room = {
                            welcomeMessage: 'Welcome!',
                            initiator: userId,
                            users: new BinarySearchTree({unique: true})
                        };
                        sendWelcomeMessage(room);
                        room.users.insert(userId, new BinarySearchTree({unique: true}));
                        rooms[message.roomId] = room;
                    }
                    connections[userId].rooms.push(message.roomId);
                    break;
                default:
                    if (roomExists(message.roomId)) {
                        if (message.userId !== userId && userExists(message.roomId, message.userId)) {
                            let remoteUserId = message.userId;
                            message.userId = userId;
                            connections[remoteUserId].ws.send(JSON.stringify(message));
                        }
                    }
            }
        });
        ws.on('close', () => {
            console.log(`[WS] User "${userId}" disconnected`);
            let roomIds = connections[userId].rooms;
            delete connections[userId];
            for (let roomId of roomIds) {
                let room = rooms[roomId];
                let isInitiator = (room.initiator === userId);
                let neighbors = getAllBSTKeys(room.users.search(userId)[0]);
                room.users.delete(userId);
                let addNeighborsToInitiator = (initiatorId) => {
                    if (neighbors.length > 0) {
                        let initiatorConnection = connections[initiatorId];
                        let initiatorNeighbors = room.users.search(initiatorId)[0];
                        for (let neighbor of neighbors) {
                            if (neighbor === initiatorId) continue;
                            try {
                                initiatorNeighbors.insert(neighbor);
                            } catch (e) {
                                continue;
                            }
                            initiatorConnection.ws.send(JSON.stringify({key: 'newUser', userId: neighbor}));
                        }
                    }
                };
                if (isInitiator) {
                    // change initiator
                    let initiatorId = room.users.key;
                    room.initiator = initiatorId;
                    console.log(`[Room "${roomId}"] New initiator: "${initiatorId}"`);
                    addNeighborsToInitiator(initiatorId);
                } else {
                    addNeighborsToInitiator(room.initiator);
                }
            }
        });
    }, (err) => {
        ws.close(1013, err.message);
    });
});

app.use(express.static('dist'));
app.set('view engine', 'hbs');

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.get('/:roomId', (req, res) => {
    let roomId = req.params.roomId;
    res.render('index', {roomId});
});