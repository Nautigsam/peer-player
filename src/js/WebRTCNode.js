'use strict';

var Backbone = require('backbone');

var RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
var IceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate;
var SessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription;

var configuration = {
    iceServers: [
        {urls: 'stun:stun.l.google.com:19302'},
        {urls: "turn:numb.viagenie.ca", credential: "webrtcdemo", username: "louis%40mozilla.com"}
    ]
};

var WebRTCConnection = function (setOnDataChannelEvent) {
    this.onICECandidate = null;
    this.onOfferCreate = null;
    this.onDataChannelOpen = null;
    this.onDataChannelClose = null;
    this.onDataMessage = null;

    this.channel = null;
    this.pc = new RTCPeerConnection(configuration);
    this.pc.onicecandidate = (ICECandidateEvent) => {
        if (this.onICECandidate && ICECandidateEvent.candidate) this.onICECandidate({candidate: ICECandidateEvent.candidate});
    };
    this.pc.onnegotiationneeded = () => {
        this.pc.createOffer()
        .then(offer => this.pc.setLocalDescription(offer))
        .then(() => {
            if (this.onOfferCreate) this.onOfferCreate({offer: this.pc.localDescription});
        });
    };
    if (setOnDataChannelEvent) {
        this.pc.ondatachannel = dataChannelEvent => this._setDataChannel(dataChannelEvent.channel);
    }

    this._setDataChannel = (channel) => {
        this.channel = channel;
        this.channel.onopen = () => {
            if (this.onDataChannelOpen) this.onDataChannelOpen();
        };
        this.channel.onclose = () => {
            if (this.onDataChannelClose) this.onDataChannelClose();
        };
        this.channel.onmessage = (e) => {
            if (this.onDataMessage) this.onDataMessage({data: e.data});
        };
    };

    this.createDataChannel = (name, options) => {
        options = options || {};
        this._setDataChannel(this.pc.createDataChannel(name, options));
    };
};

var DataMessage = function (attributes) {
    attributes = attributes || {};
    this.id = attributes.id || null;
    this.senderId = attributes.senderId || null;
    this.destId = attributes.destId || null;
    this.payload = attributes.payload || null;
    this.excludedIds = attributes.excludedIds || [];
    if (this.senderId && this.excludedIds.indexOf(this.senderId) < 0) {
        this.excludedIds.push(this.senderId);
    }
};

var DataFloodManager = function () {
    this.offset = 0;
    this.currentConnection = Promise.resolve();

    this.start = (connection, dataMessage) => {
        dataMessage.id = this.offset;
        let eventName = `dataFloodEnd:${dataMessage.id}`;
        this.currentConnection = Promise.race([
            this.currentConnection
            .catch(() => null)
            .then(() => {
                return new Promise((resolve) => {
                    this.once(eventName, resolve);
                    connection.channel.send(JSON.stringify(dataMessage));
                });
            }),
            new Promise((resolve, reject) => {
                setTimeout(reject, 5000);
            })
        ]);
        this.offset++;
        return this.currentConnection;
    };

    this.end = (id) => {
        this.trigger(`dataFloodEnd:${id}`);
        this.offset--;
    };
};
DataFloodManager.prototype = Backbone.Events;

var WebRTCNode = module.exports = function () {
    this.id = null;
    this.connections = {};
    this.floodManager = new DataFloodManager();

    this.onDataChannelOpen = null;
    this.onDataChannelClose = null;
    this.onDataMessage = null;
    this.onICECandidate = null;
    this.onOfferCreate = null;
    this.onAnswerCreate = null;

    this._onDataChannelOpenHandler = (nodeId, isInitiator) => {
        if (this.onDataChannelOpen) return this.onDataChannelOpen({nodeId, isInitiator});
    };
    this._onDataChannelCloseHandler = (nodeId) => {
        delete this.connections[nodeId];
        if (this.onDataChannelClose) return this.onDataChannelClose({nodeId});
    };
    this._onDataMessageHandler = (nodeId, event) => {
        let dataMessage = new DataMessage(JSON.parse(event.data));
        let bubbleMessage = () => {
            if (this.onDataMessage) return this.onDataMessage({nodeId, data: dataMessage.payload});
        };
        if (dataMessage.destId == null) {
            // This is a message for everybody
            this._sendDataMessageToAll(dataMessage);
            if (dataMessage.senderId !== this.id) {
                return bubbleMessage();
            }
        } else if (dataMessage.destId === this.id) {
            // This message is for us
            if (dataMessage.payload === 'ACK') {
                // The message we sent earlier with this id has been delivered
                this.floodManager.end(dataMessage.id);
            } else {
                // Send ACK message
                let ackMessage = new DataMessage({
                    id: dataMessage.id,
                    senderId: this.id,
                    destId: dataMessage.senderId,
                    payload: 'ACK'
                });
                this.connections[nodeId].channel.send(JSON.stringify(ackMessage));
                return bubbleMessage();
            }
        } else {
            // This message is for someone else
            let connection = this.connections[dataMessage.destId];
            if (connection && connection.channel) {
                connection.channel.send(event.data);
            } else {
                this._sendDataMessageToAll(dataMessage);
            }
        }
    };
    this._onICECandidateHandler = (nodeId, data) => {
        if (this.onICECandidate) return this.onICECandidate(Object.assign(data, {nodeId}));
    };
    this._onOfferCreateHandler = (nodeId, data) => {
        if (this.onOfferCreate) return this.onOfferCreate(Object.assign(data, {nodeId}));
    };
    this._onAnswerCreateHandler = (nodeId, data) => {
        if (this.onAnswerCreate) return this.onAnswerCreate(Object.assign(data, {nodeId}));
    };

    this._createEmptyConnection = (nodeId, isAnswerer) => {
        if (typeof isAnswerer === 'undefined') isAnswerer = false;

        let connection = new WebRTCConnection(isAnswerer);
        connection.onDataChannelOpen = () => this._onDataChannelOpenHandler(nodeId, !isAnswerer);
        connection.onDataChannelClose = () => this._onDataChannelCloseHandler(nodeId);
        connection.onDataMessage = event => this._onDataMessageHandler(nodeId, event);
        connection.onICECandidate = data => this._onICECandidateHandler(nodeId, data);
        connection.onOfferCreate = data => this._onOfferCreateHandler(nodeId, data);

        return connection;
    };
    this._sendDataMessageToAll = (dataMessage) => {
        for (let nodeId of Object.keys(this.connections)) {
            if (dataMessage.excludedIds.indexOf(nodeId) < 0) {
                dataMessage.excludedIds.push(nodeId);
                let connection = this.connections[nodeId];
                if (connection && connection.channel) {
                    connection.channel.send(JSON.stringify(dataMessage));
                }
            }
        }
        return Promise.resolve();
    };

    this.addConnection = (nodeId) => {
        if (this.connections.hasOwnProperty(nodeId)) return Promise.reject(new Error(`Node "${nodeId}" already connected`));

        let connection = this._createEmptyConnection(nodeId);
        connection.createDataChannel(nodeId);

        this.connections[nodeId] = connection;
        return Promise.resolve(connection);
    };

    this.setOffer = (nodeId, offer) => {
        let connection = null;
        if (!this.connections.hasOwnProperty(nodeId)) {
            connection = this._createEmptyConnection(nodeId, true);
            this.connections[nodeId] = connection;
        } else {
            connection = this.connections[nodeId];
        }
        return connection.pc.setRemoteDescription(new SessionDescription(offer))
        .then(() => connection.pc.createAnswer())
        .then(answer => connection.pc.setLocalDescription(answer))
        .then(() => {
            this._onAnswerCreateHandler(nodeId, {answer: connection.pc.localDescription});
            return connection;
        });
    };

    this.addICECandidate = (nodeId, candidate) => {
        let connection = null;
        if (!this.connections.hasOwnProperty(nodeId)) {
            connection = this._createEmptyConnection(nodeId, true);
            this.connections[nodeId] = connection;
        } else {
            connection = this.connections[nodeId];
        }
        return connection.pc.addIceCandidate(new IceCandidate(candidate))
        .then(() => connection);
    };

    this.setAnswer = (nodeId, answer) => {
        let connection = this.connections[nodeId];
        if (!connection) return Promise.reject(new Error(`No connection with node "${nodeId}"`));
        return connection.pc.setRemoteDescription(new SessionDescription(answer))
        .then(() => connection);
    };

    this.sendMessage = (nodeId, message) => {
        let dataMessage = new DataMessage({senderId: this.id, destId: nodeId, payload: message});
        if (nodeId == null) {
            return this._sendDataMessageToAll(dataMessage);
        } else {
            let connection = this.connections[nodeId];
            if (connection) {
                if (connection.channel) connection.channel.send(JSON.stringify(dataMessage));
            } else {
                return Promise.race(Object.keys(this.connections).map((neighborId) => {
                    dataMessage.excludedIds.push(neighborId);
                    return this.floodManager.start(this.connections[neighborId], dataMessage);
                }));
            }
        }
    };

};