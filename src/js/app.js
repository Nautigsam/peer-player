'use strict';

var WebRTCNode = require('./WebRTCNode');
var CURRENT_ROOM = document.getElementById('PEER_PLAYER_ROOM').innerHTML;

var ChatMessages = require('./collections/ChatMessages');
var ChatMessagesView = require('./views/ChatMessagesView');
var ChatMessage = require('./models/ChatMessage');
var ChatMessageView = require('./views/ChatMessageView');

var chatMessageBox = document.getElementById('messageBox');
var sendMessageButton = document.getElementById('sendMessage');

var chatMessagesList = new ChatMessages();
var chatMessagesListView = new ChatMessagesView({collection: chatMessagesList});

var getCurrentUsername = () => {
    return document.getElementById('username').value;
};

var createChatMessage = (content) => {
    return new ChatMessage({content, senderName: getCurrentUsername()});
};

var errorHandler = (err) => console.err(err.message);

// create web socket with server to find pairs
var ws = new WebSocket('ws://localhost:3000');
ws.onopen = () => {
    let webRTCNode = new WebRTCNode();
    webRTCNode.onDataChannelOpen = (data) => {
        console.log(`[WebRTC] Data channel opened with node "${data.nodeId}"`);
        sendMessageButton.onclick = () => {
            let message = createChatMessage(chatMessageBox.value);
            chatMessageBox.value = '';
            chatMessagesList.add(message);
            // send to everybody
            webRTCNode.sendMessage(null, JSON.stringify(message));
        }
    };
    webRTCNode.onDataChannelClose = (data) => {
        console.log(`[WebRTC] Connection lost with node "${data.nodeId}"`);
    };
    webRTCNode.onDataMessage = (data) => {
        let message = new ChatMessage(JSON.parse(data.data));
        chatMessagesList.add(message);
    };
    webRTCNode.onICECandidate = (data) => {
        ws.send(JSON.stringify({key: 'icecandidate', userId: data.nodeId, roomId: CURRENT_ROOM, value: data.candidate}));
    };
    webRTCNode.onOfferCreate = (data) => {
        ws.send(JSON.stringify({key: 'offer', userId: data.nodeId, roomId: CURRENT_ROOM, value: data.offer}));
    };
    webRTCNode.onAnswerCreate = (data) => {
        ws.send(JSON.stringify({key: 'answer', userId: data.nodeId, roomId: CURRENT_ROOM, value: data.answer}));
    };

    ws.onmessage = (messageEvent) => {
        let message = JSON.parse(messageEvent.data);
        switch (message.key) {
            case 'accept':
                console.log(`[WebRTC] Connection opened with id "${message.userId}"`);
                webRTCNode.id = message.userId;
                break;
            case 'welcome':
                chatMessagesList.add(new ChatMessage({senderName: 'Server', content: message.message}));
                break;
            case 'newUser':
                webRTCNode.addConnection(message.userId)
                .then(() => {
                    console.log(`[WebRTC] New user connected: ${message.userId}`);
                }, errorHandler);
                break;
            case 'answer':
                webRTCNode.setAnswer(message.userId, message.value)
                .then(() => {
                    console.log(`[WebRTC] Received answer from ${message.userId}`);
                }, errorHandler);
                break;
            case 'icecandidate':
                webRTCNode.addICECandidate(message.userId, message.value)
                .then(() => {
                    console.log(`[WebRTC] Received candidate from ${message.userId}`);
                }, errorHandler);
                break;
            case 'offer':
                webRTCNode.setOffer(message.userId, message.value)
                .then(() => {
                    console.log(`[WebRTC] Received offer from ${message.userId}`);
                }, errorHandler);
                break;
            default:
                break;
        }
    };
    ws.send(JSON.stringify({key: 'enterRoom', roomId: CURRENT_ROOM}));
};
ws.onclose = (e) => {
    console.error(`[WS] Connection with server closed (${e.code}): ${e.reason}`);
};