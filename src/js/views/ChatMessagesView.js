'use strict';

var Backbone = require('backbone');
var ChatMessageView = require('../views/ChatMessageView');

var ChatMessagesView = module.exports = Backbone.View.extend({
    el: '#chatMessages',

    initialize(options) {
        this.collection = options.collection;
        this.listenTo(this.collection, 'add', this.addModel);
    },

    addModel(model) {
        let modelView = new ChatMessageView({model});
        this.el.appendChild(modelView.render().el);
    },

    render() {
        this.el.innerHTML = '';
        this.collection.forEach(message => this.addModel(message));
        return this;
    }

});