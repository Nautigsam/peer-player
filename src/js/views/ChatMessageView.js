'use strict';

var Backbone = require('backbone');
var ChatMessage = require('../models/ChatMessage');
var ChatMessageTemplate = require('../../templates/ChatMessage.handlebars');

var ChatMessageView = module.exports = Backbone.View.extend({
    tagName: 'li',
    className: 'chatMessage',
    model: ChatMessage,
    template: ChatMessageTemplate,

    render() {
        this.el.innerHTML = this.template(this.model.attributes);
        return this;
    }
});