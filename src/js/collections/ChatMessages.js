'use strict';

var Backbone = require('backbone');
var ChatMessage = require('../models/ChatMessage');

var ChatMessages = module.exports = Backbone.Collection.extend({
    model: ChatMessage
});